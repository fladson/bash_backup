export GREP_OPTIONS="--color=auto"
export GREP_COLOR="4;33"
export CLICOLOR="auto"

# aliases
alias ls="ls -G"
alias cd..="cd .."
alias l="ls -al"
alias lp="ls -p"
alias l.='ls -d .* --color=auto'
alias H="cd $HOME"
alias freq='cut -f1 -d" " ~/.bash_history | sort | uniq -c | sort -nr | head -n 30'

alias h=history
alias bashrf="source /Users/fladson/.bash_profile"
alias speedup="sudo rm -rf /private/var/log/asl/*.asl"
alias ip="curl ifconfig.me"
alias fixow='/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -kill -r -domain local -domain user;killall Finder;echo "Open With has been rebuilt, Finder will relaunch"'
alias bi='bundle install'

# git aliases
alias gwc='git whatchanged -p --abbrev-commit --pretty=medium'
alias gst='git status'
alias gc='git commit -m'
alias gca='git commit -v -a'
alias gco='git checkout'
alias gb='git branch'
alias gl="git log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias lg='git log --graph --full-history --all --color --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s"'
alias gitref='cat ~/.bash_profile ~/.git_commands | grep git'

# don't put duplicate lines in the history.
HISTCONTROL=ignoredups:ignorespace


source $HOME/.bash/general.sh
source $HOME/.bash/prompt.sh
source $HOME/.bash/gem_completion.sh
source $HOME/.bash/git_completion.sh

if [ -f /etc/bash_completion ]; then
  source /etc/bash_completion
fi

if [ -f /usr/local/etc/bash_completion ]; then
  source /usr/local/etc/bash_completion
fi

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
# PS1='\n${USERNAME}${BASE_COLOR}\u ${LOCATION}\w\n${SNOWMAN} ¬ ${TEXT}'
